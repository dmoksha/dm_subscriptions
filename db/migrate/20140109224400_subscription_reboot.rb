# frozen_string_literal: true

class SubscriptionReboot < ActiveRecord::Migration
  def up
    drop_table      :sub_subscription_plans
    drop_table      :sub_user_subscriptions
    remove_column   :user_site_profiles, :customer_token
  end

  def down; end
end
