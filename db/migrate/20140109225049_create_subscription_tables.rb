# frozen_string_literal: true

class CreateSubscriptionTables < ActiveRecord::Migration
  def up
    create_table 'subscription_affiliates', force: true do |t|
      t.string   'name'
      t.decimal  'rate', precision: 6, scale: 4, default: 0.0
      t.datetime 'created_at'
      t.datetime 'updated_at'
      t.string   'token'
      t.integer  'account_id'
    end

    add_index 'subscription_affiliates', ['token'], name: 'index_subscription_affiliates_on_token'
    add_index 'subscription_affiliates', ['account_id']

    create_table 'subscription_discounts', force: true do |t|
      t.string   'name'
      t.string   'code'
      t.money    'amount'
      t.boolean  'percent'
      t.date     'start_on'
      t.date     'end_on'
      t.datetime 'created_at'
      t.datetime 'updated_at'
      t.boolean  'apply_to_setup',                                       default: true
      t.boolean  'apply_to_recurring',                                   default: true
      t.integer  'trial_period_extension',                               default: 0
      t.integer  'account_id'
    end

    add_index 'subscription_discounts', ['account_id']

    create_table 'subscription_payments', force: true do |t|
      t.integer  'subscription_id'
      t.money    'amount'
      t.string   'description'
      t.string   'transaction_id'
      t.datetime 'created_at'
      t.datetime 'updated_at'
      t.boolean  'setup'
      t.boolean  'misc'
      t.integer  'subscription_affiliate_id'
      t.money    'affiliate_amount'
      t.integer  'subscriber_id'
      t.string   'subscriber_type'
      t.string   'payment_processor', limit: 20
      t.string   'card_number'
      t.integer  'invoice_num'
      t.string   'invoice_filename'
      t.integer  'account_id'
    end

    add_index 'subscription_payments', %w[subscriber_id subscriber_type], name: 'index_payments_on_subscriber'
    add_index 'subscription_payments', ['subscription_id'], name: 'index_subscription_payments_on_subscription_id'
    add_index 'subscription_payments', ['invoice_num']
    add_index 'subscription_payments', ['account_id']

    create_table 'subscription_plans', force: true do |t|
      t.string   'slug'
      t.money    'price'
      t.datetime 'created_at'
      t.datetime 'updated_at'
      t.integer  'renewal_period', default: 1
      t.integer  'setup_price_cents'
      t.integer  'trial_period',                                  default: 1
      t.string   'trial_interval',                                default: 'months'
      t.integer  'row_order'
      t.integer  'account_id'
      # t.integer  "user_limit"                 sample "_limit" field
    end

    add_index 'subscription_plans', ['account_id']
    add_index 'subscription_plans', ['slug']

    create_table :subscription_plan_translations, force: true do |t|
      t.integer     :subscription_plan_id
      t.string      :locale
      t.string      :name
    end

    create_table 'subscriptions', force: true do |t|
      t.money    'amount'
      t.datetime 'next_renewal_at'
      t.datetime 'paid_until'
      t.string   'card_number'
      t.string   'card_expiration'
      t.datetime 'created_at'
      t.datetime 'updated_at'
      t.string   'state', default: 'trial'
      t.integer  'subscription_plan_id'
      t.integer  'subscriber_id'
      t.string   'subscriber_type'
      t.integer  'renewal_period', default: 1
      t.string   'billing_id'
      t.integer  'subscription_discount_id'
      t.integer  'subscription_affiliate_id'
      t.string   'payment_processor', limit: 20
      t.string   'name_on_card'
      t.integer  'account_id'
      # t.integer  "user_limit"                 sample "_limit" field
    end

    add_index 'subscriptions', %w[subscriber_id subscriber_type], name: 'index_subscriptions_on_subscriber'
    add_index 'subscriptions', ['account_id']
  end

  def down
    drop_table 'subscription_affiliates'
    drop_table 'subscription_discounts'
    drop_table 'subscription_payments'
    drop_table 'subscription_plans'
    drop_table 'subscriptions'
  end
end
