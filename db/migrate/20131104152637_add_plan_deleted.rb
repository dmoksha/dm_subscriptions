# frozen_string_literal: true

class AddPlanDeleted < ActiveRecord::Migration
  def change
    add_column    :sub_subscription_plans,    :deleted,     :boolean, default: false
    add_column    :sub_subscription_plans,    :account_id,  :integer
  end
end
