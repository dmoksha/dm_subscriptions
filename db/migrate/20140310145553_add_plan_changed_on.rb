# frozen_string_literal: true

class AddPlanChangedOn < ActiveRecord::Migration
  def up
    add_column :subscriptions, :plan_changed_on, :datetime
    Subscription.unscoped.all.each do |sub|
      Account.current = Account.find(sub.account_id)
      sub.update_attribute(:plan_changed_on, sub.created_at)
    end
  end

  def down
    remove_column :subscriptions, :plan_changed_on
  end
end
