# frozen_string_literal: true

class CreateSubscriptionPlan < ActiveRecord::Migration
  def change
    create_table :sub_subscription_plans do |t|
      t.string    :name
      t.string    :vendor_plan_id
      t.integer   :amount_cents
      t.string    :currency, limit: 3
      t.string    :interval
      t.integer   :trial_period_days

      t.timestamps null: true
    end
  end
end
