# frozen_string_literal: true

class CreateUserSubscriptions < ActiveRecord::Migration
  def change
    create_table :sub_user_subscriptions do |t|
      t.integer   :subscription_plan_id
      t.integer   :user_id
      t.integer   :account_id
      t.string    :last_4_digits
      t.string    :vault_token
      t.timestamps null: true
    end
  end
end
