# frozen_string_literal: true

class DmSubscriptions::Admin::SubscriptionsController < DmSubscriptions::Admin::AdminController
  include DmSubscriptions::PermittedParams

  before_action   :plan_lookup
  before_action   :subscription_lookup, except: %i[new create]

  #------------------------------------------------------------------------------
  def new
    @subscription = @subscription_plan.subscriptions.build
  end

  #------------------------------------------------------------------------------
  def edit; end

  #------------------------------------------------------------------------------
  def create
    @subscription = @subscription_plan.subscriptions.new(subscription_params)
    @subscription.subscriber = current_user
    if @subscription.save
      redirect_to admin_subscription_plan_path(@subscription_plan), notice: 'Subscription was successfully created.'
    else
      render action: :new
    end
  end

  #------------------------------------------------------------------------------
  def update
    if @subscription.update_attributes(subscription_params)
      redirect_to admin_subscription_plan_path(@subscription_plan), notice: 'Subscription was successfully updated.'
    else
      render action: :edit
    end
  end

  # [todo]
  #------------------------------------------------------------------------------
  def destroy
    # @forum.destroy
    # redirect_to admin_forum_category_url(@forum.forum_category)
  end

  #------------------------------------------------------------------------------
  def make_payment
    if put_or_post?
      @subscription.billing_address.present? ? @subscription.billing_address.update_attributes(make_payment_billing_address_params) : @subscription.create_billing_address(make_payment_billing_address_params)
      @subscription.update_attribute(:name_on_card, params[:make_payment][:name_on_card])
      amount = Monetize.parse(params[:make_payment][:amount], @subscription.amount.currency)
      if @subscription.manual_payment(amount)
        redirect_to admin_subscription_plan_path(@subscription_plan), notice: 'Payment made'
      end
    else
      @subscription.build_billing_address if @subscription.billing_address.nil?
    end
  end

  private

  #------------------------------------------------------------------------------
  def plan_lookup
    @subscription_plan = SubscriptionPlan.friendly.find(params[:subscription_plan_id])
  end

  #------------------------------------------------------------------------------
  def subscription_lookup
    @subscription = @subscription_plan.subscriptions.find(params[:id])
  end
end
