# frozen_string_literal: true

# Subclass from main ApplicationController, which will subclass from DmCore
#------------------------------------------------------------------------------
class DmSubscriptions::ApplicationController < ::ApplicationController
  layout 'subscriptions'
end
