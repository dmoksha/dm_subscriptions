# frozen_string_literal: true

# Typically used in a Devise::RegistrationController.
#------------------------------------------------------------------------------
module DmSubscriptions
  module Concerns
    module PlanSignupController
      extend ActiveSupport::Concern

      included do
        before_action       :load_affiliate,      only: [:create]
        before_action       :load_discount,       only: %i[new create]
        before_action       :load_plan,           only: %i[new create]
        after_action        :build_plan,          only: [:create]
        skip_before_action  :collect_billing_info
      end

      protected

      # it's ok if the plan is not found - means they are not subscribed
      #------------------------------------------------------------------------------
      def load_plan
        if params[:plan].present?
          @plan = SubscriptionPlan.friendly.find(params[:plan])
        elsif params[:user].present? && params[:user][:plan].present?
          @plan = SubscriptionPlan.friendly.find(params[:user][:plan])
        end
        @plan.discount = @discount if @plan
      end

      # it's ok if the plan is not found - means they are not subscribed
      #------------------------------------------------------------------------------
      def build_plan
        if resource.present?
          @plan           = SubscriptionPlan.friendly.find(params[:user][:plan])
          @plan.discount  = @discount if @plan
        end
      end

      # Load the discount by code, but not if it's not available
      #------------------------------------------------------------------------------
      def load_discount
        if params[:discount].blank? || !(@discount = SubscriptionDiscount.find_by_code(params[:discount])) || !@discount.available?
          @discount = nil
        end
      end

      # Load the discount by code, but not if it's not available
      #------------------------------------------------------------------------------
      def load_affiliate
        @affiliate = cookies[:affiliate] || nil
      end

      module ClassMethods
      end
    end
  end
end
