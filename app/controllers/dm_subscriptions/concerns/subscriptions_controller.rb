# frozen_string_literal: true

module DmSubscriptions
  module Concerns
    module SubscriptionsController
      extend ActiveSupport::Concern

      protected

      #------------------------------------------------------------------------------
      def store_billing
        #--- used for customer/client in Stripe/Paymill
        email               = current_user.email
        description         = current_user.full_name
        @address.first_name = @creditcard.first_name
        @address.last_name  = @creditcard.last_name

        #--- store the billing address in subscription
        unless @subscription.billing_address.present?
          @subscription.create_billing_address
        end
        @subscription.update_attributes(name_on_card: "#{@address.first_name} #{@address.last_name}")
        @subscription.billing_address.update_attributes(line1: @address.address1, line2: @address.address2,
                                                        city: @address.city, state: @address.state,
                                                        zip: @address.zip, country_code: @address.country)

        result = if params[:card_token].present?
                   @subscription.store_card(params[:card_token], email: email, description: description)
                 else
                   (@creditcard.valid? & @address.valid?) && @subscription.store_card(@creditcard, email: email, description: description, billing_address: @address.to_activemerchant, ip: request.remote_ip)
        end
        if result
        end
        result
      end

      #------------------------------------------------------------------------------
      def load_billing
        @creditcard = ActiveMerchant::Billing::CreditCard.new(params[:creditcard] || {})
        @address    = SubscriptionAddress.new(params[:address])
      end

      #------------------------------------------------------------------------------
      def load_subscription
        @subscription = current_user.subscription if current_user
      end

      module ClassMethods
      end
    end
  end
end
