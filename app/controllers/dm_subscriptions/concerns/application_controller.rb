# frozen_string_literal: true

module DmSubscriptions
  module Concerns
    module ApplicationController
      extend ActiveSupport::Concern

      included do
        before_action :set_affiliate_cookie
        before_action :collect_billing_info
      end

      #------------------------------------------------------------------------------
      def set_affiliate_cookie
        if !params[:ref].blank? && affiliate = SubscriptionAffiliate.find_by_token(params[:ref])
          cookies[:affiliate] = { value: params[:ref], expires: 1.month.from_now }
        end
      end

      # Redirect to the billing page if the user needs to enter
      # payment info before being able to use the app
      #------------------------------------------------------------------------------
      def collect_billing_info
        return unless user_signed_in?
        return if self.class.to_s.match(/^Devise/)      # Don't prevent logins, etc.
        return if current_user.is_admin?                # Admins don't need to pay
        # return unless account = current_account       # Nothing to do if there is no account

        return if !(sub = current_user.subscription) || sub.state.nil?
        if sub.state == 'active' && sub.current? && !sub.needs_payment_info?
          return
        end
        return if sub.state == 'active' && sub.amount == 0
        if sub.state == 'trial' && sub.current? && (!DmSubscriptions.config.require_payment_info_for_trials || !sub.needs_payment_info?)
          return
        end

        redirect_to main_app.subscription_billing_path
      end

      module ClassMethods
      end
    end
  end
end
