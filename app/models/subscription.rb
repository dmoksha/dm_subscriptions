# frozen_string_literal: true

# paid_until was added to indicate that a subscription is paid up until
# a certain date.  This is particularly important when downgrading a paid plan
# to a lesser plan (free or shorter payment period) - user should have full access
# until the paid period expires - no refunds are offered.
#------------------------------------------------------------------------------
class Subscription < ApplicationRecord
  monetize          :amount_cents, with_model_currency: :amount_currency

  belongs_to        :subscriber, polymorphic: true
  belongs_to        :subscription_plan
  has_many          :subscription_payments
  belongs_to        :discount, class_name: 'SubscriptionDiscount', foreign_key: 'subscription_discount_id'
  belongs_to        :affiliate, class_name: 'SubscriptionAffiliate', foreign_key: 'subscription_affiliate_id'
  belongs_to        :account
  has_one           :billing_address, as: :addressable, class_name: 'Address'

  before_create     :set_renewal_at
  before_update     :apply_discount
  before_destroy    :destroy_gateway_record

  after_save        :set_subscription_role
  after_destroy     :set_subscription_role

  attr_accessor     :creditcard, :address
  attr_reader       :response

  # renewal_period is the number of months to bill at a time
  # default is 1
  validates_numericality_of :renewal_period, only_integer: true, greater_than: 0
  validates_numericality_of :amount, greater_than_or_equal_to: 0
  validate                  :card_storage, on: :create
  validate                  :within_limits, on: :update

  default_scope             { where(account_id: Account.current.id) }
  scope                     :pastdue_renewals, ->(days) { where('next_renewal_at < ? AND paid_until > NULL', days) }
  scope                     :fully_paid, -> { where("state = 'active' AND paid_until > ?", Time.now) }
  scope                     :lapsed, -> { where('paid_until <= ?', Time.now) }
  scope                     :never_paid, -> { where('paid_until IS NULL') }

  # Changes the subscription plan, and assigns various properties,
  # such as limits, etc., to the subscription from the assigned
  # plan.
  #
  # When adding new limits that are specified in SubscriptionPlan,
  # if you name them like "some_quantity_limit", they will automatically
  # be used by this method.
  #
  # Otherwise, you'll need to manually add the assignment to this method.
  #
  # Allow even free accounts to have a trial period (for tasting the full version)
  #------------------------------------------------------------------------------
  def plan=(plan)
    if plan.amount.positive?
      # Discount the plan with the existing discount (if any)
      # if the plan doesn't already have a better discount
      plan.discount = discount if discount && discount > plan.discount

      # If the assigned plan has a better discount, though, then
      # assign the discount to the subscription so it will stick
      # through future plan changes
      self.discount = plan.discount if plan.discount && plan.discount > discount

      # if going from free -> paid, not a trial, and no outstanding paid amount,
      # then set  next_renewal_at to now so that billing can start
      if amount.zero? && !trial? && !currently_paid?
        self.next_renewal_at = Time.now
      end
    else
      # make sure an initial free plan with no trial period is immediately active
      # self.state = 'active' if new_record? && plan.trial_period == 0
    end
    self.state = 'active' if new_record? && plan.trial_period == 0
    self.plan_changed_on = Time.now

    #
    # Find any attributes that exist in both the Subscription and SubscriptionPlan
    # and that match the pattern of "something_limit"
    #
    limits = attributes.keys.select { |k| k =~ /^.+_limit$/ } &
             plan.attributes.keys.select { |k| k =~ /^.+_limit$/ }

    (limits + %i[amount renewal_period]).each do |f|
      send("#{f}=", plan.send(f))
    end

    self.subscription_plan = plan
  end

  # The plan_id and plan_id= methods are convenience methods for the
  # administration interface.
  #------------------------------------------------------------------------------
  def plan_id
    subscription_plan_id
  end

  #------------------------------------------------------------------------------
  def plan_id=(a_plan_id)
    if a_plan_id.to_i != subscription_plan_id
      self.plan = SubscriptionPlan.find(a_plan_id)
    end
  end

  #------------------------------------------------------------------------------
  def inactive?
    state == 'inactive'
  end

  #------------------------------------------------------------------------------
  def active?
    state == 'active'
  end

  #------------------------------------------------------------------------------
  def trial?
    state == 'trial'
  end

  #------------------------------------------------------------------------------
  def trial_days
    (next_renewal_at.to_i - Time.now.to_i) / 86_400
  end

  #------------------------------------------------------------------------------
  def store_card(creditcard, gw_options = {})
    @response = if billing_id.blank?
                  payment_api.gateway.store(creditcard, gw_options)
                else
                  payment_api.gateway.update(billing_id, creditcard, gw_options)
    end

    if @response.success?
      if creditcard.is_a? String
        extracted_card        = payment_api.extract_card(response)
        self.card_number      = extracted_card[:display_number]
        self.card_expiration  = extracted_card[:expiration]
      else
        self.card_number      = creditcard.display_number
        self.card_expiration  = format('%02d-%d', creditcard.expiry_date.month, creditcard.expiry_date.year)
      end
      set_billing
    else
      errors.add(:base, @response.message)
      false
    end
  end

  #------------------------------------------------------------------------------
  def payment_description(start_date = nil)
    date = if start_date
             start_date
           else
             next_renewal_at.present? ? next_renewal_at : Time.now
    end

    I18n.t('subscriptions.invoice_description', plan_name: subscription_plan.name_en, from_date: (date + 1.day).to_date.to_s(:long), to_date: date.advance(months: renewal_period).to_date.to_s(:long))
  end

  # Charge the card on file the amount stored for the subscription
  # record.  This is called by the daily_mailer script for each
  # subscription that is due to be charged.  A SubscriptionPayment
  # record is created, and the subscription's next renewal date is
  # set forward when the charge is successful.
  #------------------------------------------------------------------------------
  def charge
    description = payment_description
    if amount.zero? || (@response = payment_api.gateway.purchase(amount.cents, billing_id, currency: amount.currency, order_id: description)).success?
      unless amount.zero?
        update_attribute(:paid_until, next_renewal_at.advance(months: renewal_period))
      end
      activate_subscription
      unless amount.zero?
        subscription_payments.create(subscriber: subscriber,
                                     amount: amount,
                                     transaction_id: @response.authorization,
                                     payment_processor: payment_api.processor,
                                     description: description,
                                     card_number: card_number)
      end
      true
    else
      errors.add(:base, @response.message)
      false
    end
  end

  # This is for making a manual subscription payment (like if it came in via wire
  # transfer or something, and we just need to record it and send the invoice)
  # no matter the amount, this assumes that the subscription is renewed for the
  # full renewal period
  #------------------------------------------------------------------------------
  def manual_payment(manual_amount = amount)
    description = payment_description
    unless manual_amount.zero?
      update_attribute(:paid_until, next_renewal_at.advance(months: renewal_period))
    end
    activate_subscription
    unless manual_amount.zero?
      subscription_payments.create(subscriber: subscriber,
                                   amount: manual_amount,
                                   transaction_id: nil,
                                   payment_processor: 'manual',
                                   description: description)
    end
    true
  end

  # Charge the card on file any amount you want.  Pass in a dollar
  # amount (1.00 to charge $1).  A SubscriptionPayment record will
  # be created, but the subscription itself is not modified.
  #------------------------------------------------------------------------------
  def misc_charge(amount)
    if amount.zero? || (@response = payment_api.gateway.purchase(amount.cents, billing_id, currency: amount.currency)).success?
      subscription_payments.create(subscriber: subscriber,
                                   amount: amount,
                                   transaction_id: @response.authorization,
                                   payment_processor: payment_api.processor,
                                   card_number: card_number,
                                   misc: true)
      true
    else
      errors.add(:base, @response.message)
      false
    end
  end

  #------------------------------------------------------------------------------
  def needs_payment_info?
    card_number.blank? && subscription_plan.amount.positive?
  end

  # Activate the subscription
  #------------------------------------------------------------------------------
  def activate_subscription
    update_attributes(next_renewal_at: next_renewal_at.advance(months: renewal_period), state: 'active')
  end

  #------------------------------------------------------------------------------
  def self.find_expiring_trials(renew_at = 7.days.from_now)
    includes(:subscriber).where(state: 'trial', next_renewal_at: (renew_at.beginning_of_day..renew_at.end_of_day))
  end

  #------------------------------------------------------------------------------
  def self.find_due_trials(renew_at = Time.now)
    includes(:subscriber).where(state: 'trial', next_renewal_at: (renew_at.beginning_of_day..renew_at.end_of_day)).reject { |s| s.card_number.blank? }
  end

  #------------------------------------------------------------------------------
  def self.find_due(renew_at = Time.now)
    includes(:subscriber).where(state: 'active', next_renewal_at: (renew_at.beginning_of_day..renew_at.end_of_day))
  end

  # subscription is current (not lapsed)
  #------------------------------------------------------------------------------
  def current?
    next_renewal_at >= Time.now
  end

  # subscription is paid up until now
  #------------------------------------------------------------------------------
  def currently_paid?
    paid_until && paid_until >= Time.now
  end

  #------------------------------------------------------------------------------
  def to_s
    "#{card_number} - #{card_expiration}"
  end

  protected

  #------------------------------------------------------------------------------
  def set_billing
    self.billing_id ||= @response.authorization

    if new_record?
      if !next_renewal_at? || next_renewal_at < 1.day.from_now.at_midnight
        if subscription_plan.trial_period?
          self.next_renewal_at = Time.now.advance(subscription_plan.trial_interval.to_sym => subscription_plan.trial_period)
        else
          charge_amount = subscription_plan.setup_price.zero? ? amount : subscription_plan.setup_amount
          description   = payment_description(Time.now.advance(months: renewal_period))
          if (@response = payment_api.gateway.purchase(charge_amount.cents, billing_id, currency: charge_amount.currency, order_id: description)).success?
            subscription_payments.build(subscriber: subscriber,
                                        amount: charge_amount,
                                        transaction_id: @response.authorization,
                                        setup: !subscription_plan.setup_price.zero?,
                                        payment_processor: payment_api.processor,
                                        description: description,
                                        card_number: card_number)
            self.state              = 'active'
            self.next_renewal_at    = Time.now.advance(months: renewal_period)
            self.paid_until         = next_renewal_at
          else
            errors.add(:base, @response.message)
            return false
          end
        end
      end
    else
      if !next_renewal_at? || next_renewal_at < 1.day.from_now.at_midnight
        description   = payment_description(Time.now.advance(months: renewal_period))
        if (@response = payment_api.gateway.purchase(amount.cents, billing_id, currency: amount.currency, order_id: description)).success?
          subscription_payments.build(subscriber: subscriber,
                                      amount: amount,
                                      transaction_id: @response.authorization,
                                      setup: false,
                                      payment_processor: payment_api.processor,
                                      description: description,
                                      card_number: card_number)
          self.state              = 'active'
          self.next_renewal_at    = Time.now.advance(months: renewal_period)
          self.paid_until         = next_renewal_at
        else
          errors.add(:base, @response.message)
          return false
        end
      else
        self.state = 'active'
      end
      save
    end

    true
  end

  #------------------------------------------------------------------------------
  def set_renewal_at
    return if subscription_plan.nil? || next_renewal_at

    if subscription_plan.trial_period?
      self.next_renewal_at = Time.now.advance(subscription_plan.trial_interval.to_sym => subscription_plan.trial_period)
    elsif subscription_plan.amount.positive?
      # no trial period, and amount is due - payment info needs to be collected
      self.next_renewal_at = Time.now
    else
      self.next_renewal_at = Time.now.advance(months: renewal_period)
    end
  end

  # If the discount is changed, set the amount to the discounted
  # plan amount with the new discount.
  #------------------------------------------------------------------------------
  def apply_discount
    if subscription_discount_id_changed?
      subscription_plan.discount = discount
      self.amount = subscription_plan.amount
    end
  end

  #------------------------------------------------------------------------------
  def payment_api
    @payment_api ||= DmSubscriptions::PaymentApi.api
  end

  #------------------------------------------------------------------------------
  def destroy_gateway_record
    return if billing_id.blank?

    payment_api.gateway.unstore(billing_id)
    clear_billing_info
  end

  #------------------------------------------------------------------------------
  def clear_billing_info
    self.card_number = nil
    self.card_expiration = nil
    self.billing_id = nil
  end

  #------------------------------------------------------------------------------
  def card_storage
    if @creditcard && @address && card_number.blank?
      store_card(@creditcard, billing_address: @address.to_activemerchant)
    end
  end

  #------------------------------------------------------------------------------
  def within_limits
    return unless subscription_plan_id_changed?

    subscriber.class.subscription_limits.each do |limit, rule|
      unless (cap = subscription_plan.send(limit)).nil? || rule.call(subscriber) <= cap
        errors.add(:base, "#{limit.to_s.humanize} for new plan would be exceeded.")
      end
    end
  end

  # Set special role on the 'subscriber'.  This way various subsystems can simply
  # check for this role without requiring the subscription subsystem to be included
  #------------------------------------------------------------------------------
  def set_subscription_role
    if subscriber
      if !destroyed? && active? && currently_paid? && current?
        subscriber.add_role :paid_subscription
      else
        subscriber.remove_role :paid_subscription
      end
    end
  end
end
