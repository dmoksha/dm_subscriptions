# frozen_string_literal: true

class SubscriptionDiscount < ApplicationRecord
  include Comparable
  class ComparableError < StandardError; end

  monetize                    :amount_cents, with_model_currency: :amount_currency

  validates_numericality_of   :amount
  validates_presence_of       :code, :name

  before_save                 :check_percentage

  attr_accessor               :calculated_amount

  default_scope               { where(account_id: Account.current.id) }

  #------------------------------------------------------------------------------
  def available?
    return false if start_on && start_on > Time.now.utc.to_date
    return false if end_on && end_on < Time.now.utc.to_date

    true
  end

  #------------------------------------------------------------------------------
  def calculate(subtotal)
    return 0.to_money(subtotal.currency) unless subtotal.positive?
    return 0.to_money(subtotal.currency) unless available?

    self.calculated_amount = if percent
                               subtotal * amount.to_f
                             else
                               amount > subtotal ? subtotal : amount
    end
  end

  #------------------------------------------------------------------------------
  def <=>(other)
    return 1 if other.nil?
    if percent != other.percent
      raise ComparableError, "Can't compare discounts that are calculated differently"
    end

    amount <=> other.amount
  end

  protected

  #------------------------------------------------------------------------------
  def check_percentage
    self.amount = amount / 100 if amount.positive? && percent
  end
end
