# frozen_string_literal: true

# Abstract out access to ActiveMerchant and various processor specific
# helper functions for Paymill
#------------------------------------------------------------------------------
class DmSubscriptions::PaymentApiPaymill < DmSubscriptions::PaymentApi
  # Since the Paymill api uses an api_key that must be set once for the whole api,
  # the proper method is overridden in the initializer.  This is here for
  # completeness
  #------------------------------------------------------------------------------
  def api_key
    Rails.application.secrets[Account.current.account_prefix]['paymill_private_key']
  end

  #------------------------------------------------------------------------------
  def public_key
    Rails.application.secrets[Account.current.account_prefix]['paymill_public_key']
  end

  #------------------------------------------------------------------------------
  def processor
    'paymill'
  end

  #------------------------------------------------------------------------------
  def paymill?
    true
  end

  # indicates if this gateway uses a javascript bridge
  #------------------------------------------------------------------------------
  def javascript_bridge?
    true
  end

  #------------------------------------------------------------------------------
  def gateway
    @gateway ||= ActiveMerchant::Billing::Base.gateway(self.processor).new(private_key: self.api_key, public_key: self.public_key)
  end

  # Extracts the last 4 digits and exp from response once card has been stored
  #------------------------------------------------------------------------------
  def extract_card(response)
    active_card = response.params['data']['payment'].last
    {display_number: "XXXX-XXXX-XXXX-#{active_card['last4']}", expiration: "%02d-%d" % [active_card['expire_month'], active_card['expire_year']]}
  end

  module Card
    # Get a test credit card token
    #------------------------------------------------------------------------------
    def self.test_token
      key = DmSubscriptions::PaymentApiPaymill.new.public_key
      raise 'Paymill public key not set' unless key.present?

      # Simulate the JavaScript bridge we would use in production
      params = {
        'transaction.mode'        => 'CONNECTOR_TEST',
        'channel.id'              => key,
        'jsonPFunction'           => 'any_string',
        'account.number'          => '4111111111111111',
        'account.expiry.month'    => sprintf("%02d", 3.years.from_now.month),
        'account.expiry.year'     => 3.years.from_now.year,
        'account.verification'    => '111',
        'presentation.amount3D'   => 3201,
        'presentation.currency3D' => 'EUR'
      }
      http = Net::HTTP.new('test-token.paymill.de', 443)
      http.use_ssl = true
      response = http.get params.url_query_string
      response.body.scan(/tok_\w*\b/).first # Use regex to pull token from (not-quite-JSON) response
    end

  end

  # # subscription plans
  # #------------------------------------------------------------------------------
  # def self.plans(options = {})
  #   result = Paymill::Offer.all
  #   plans = []
  #   result.each do |plan|
  #     plans << {vendor_plan_id: plan.id, name: plan.name, amount_cents: plan.amount, currency: plan.currency,
  #               interval: plan.interval, trial_period_days: plan.trial_period_days }
  #   end
  #   return plans
  # end
  #
  # module Customer
  #   #------------------------------------------------------------------------------
  #   def self.find(client_id)
  #     Paymill::Client.find(client_id)
  #   end
  #
  #   # Create a new client object and return it's id/token
  #   #------------------------------------------------------------------------------
  #   def self.create(options = {email: '', description: ''})
  #     client = Paymill::Client.create(email: options[:email], description: options[:description])
  #     client.id
  #   end
  # end
  #
  # module Subscription
  #
  #   # create a new subscription to the specified plan, and return the subscirption id
  #   #------------------------------------------------------------------------------
  #   def self.create(options = { plan: '', customer_token: '', card_token: '' })
  #     payment       = Paymill::Payment.create token: options[:card_token], client: options[:customer_token]
  #     subscription  = Paymill::Subscription.create offer: options[:plan], client: options[:customer_token], payment: payment.id
  #     subscription.id
  #   end
  #
  # end
  #
end
