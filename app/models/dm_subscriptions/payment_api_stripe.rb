# frozen_string_literal: true

# Abstract out access to ActiveMerchant and various processor specific
# helper functions for Stripe
#------------------------------------------------------------------------------
require 'stripe'
class DmSubscriptions::PaymentApiStripe < DmSubscriptions::PaymentApi
  # Stripe api supports passing in the api key to each call.  Pull the key from
  # the Account
  #------------------------------------------------------------------------------
  def api_key
    Rails.application.secrets[Account.current.account_prefix]['stripe_private_key']
  end

  #------------------------------------------------------------------------------
  def public_key
    Rails.application.secrets[Account.current.account_prefix]['stripe_public_key']
  end

  #------------------------------------------------------------------------------
  def processor
    'stripe'
  end

  #------------------------------------------------------------------------------
  def stripe?
    true
  end

  # indicates if this gateway uses a javascript bridge
  #------------------------------------------------------------------------------
  def javascript_bridge?
    true
  end

  #------------------------------------------------------------------------------
  def gateway
    @gateway ||= ActiveMerchant::Billing::Base.gateway(processor).new(login: api_key, password: '')
  end

  # Extracts the last 4 digits and exp from response once card has been stored
  #------------------------------------------------------------------------------
  def extract_card(response)
    active_card = response.params['sources'] ? response.params['sources']['data'].first : response.params['cards']['data'].first
    { display_number: "XXXX-XXXX-XXXX-#{active_card['last4']}", expiration: format('%02d-%d', active_card['exp_month'], active_card['exp_year']) }
  end

  module Card
    # Create a test credit card token
    #------------------------------------------------------------------------------
    def self.test_token
      Stripe::Token.create(
        { card: {
          number: '4242424242424242',
          exp_month: 12,
          exp_year: 2020,
          cvc: '314'
        } }, DmSubscriptions::PaymentApiStripe.new.api_key
      ).id
    end
  end

  # # subscription plans
  # #------------------------------------------------------------------------------
  # def self.plans(options = {})
  #   result = Stripe::Plan.all({}, api_key)
  #   plans = []
  #   result[:data].each do |plan|
  #     plans << {vendor_plan_id: plan[:id], name: plan[:name], amount_cents: plan[:amount], currency: plan[:currency],
  #               interval: plan[:interval], trial_period_days: plan[:trial_period_days] }
  #   end
  #   return plans
  # end
  #
  # module Customer
  #   #------------------------------------------------------------------------------
  #   def self.find(customer_token)
  #     Stripe::Customer.retrieve(customer_token, DmSubscriptions::PaymentApiStripe.api_key)
  #   end
  #
  #   # Create a new customer object and return it's id/token
  #   #------------------------------------------------------------------------------
  #   def self.create(options = {email: '', description: ''})
  #     customer = Stripe::Customer.create({email: options[:email], description: options[:description]}, DmSubscriptions::PaymentApiStripe.api_key)
  #     customer.id
  #   end
  # end
  #

  # module Subscription
  #
  #   # create a new subscription to the specified plan, and return the subscirption id
  #   #------------------------------------------------------------------------------
  #   def self.create(options = { plan: '', customer_token: '', card_token: '' })
  #     customer      = Customer.find(options[:customer_token])
  #     card          = customer.cards.create(card: options[:card_token])
  #     subscription  = customer.update_subscription(plan: options[:plan])
  #     subscription.id
  #   end
  # end
end
