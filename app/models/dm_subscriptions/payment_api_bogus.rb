# frozen_string_literal: true

# Abstract out access to ActiveMerchant and various processor specific
# helper functions for a bogus payment method (for testing)
#------------------------------------------------------------------------------
class DmSubscriptions::PaymentApiBogus < DmSubscriptions::PaymentApi
  #------------------------------------------------------------------------------
  def api_key
    ''
  end

  #------------------------------------------------------------------------------
  def public_key
    ''
  end

  #------------------------------------------------------------------------------
  def processor
    'bogus'
  end

  #------------------------------------------------------------------------------
  def gateway
    @gateway ||= ActiveMerchant::Billing::Base.gateway(:bogus).new
  end

  module Card
    # Create a test credit card token
    #------------------------------------------------------------------------------
    def self.test_token
      ''
    end
  end
end
