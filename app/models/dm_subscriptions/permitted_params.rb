# frozen_string_literal: true

module DmSubscriptions
  module PermittedParams
    #------------------------------------------------------------------------------
    def subscription_plan_params
      if can? :manage_subscriptions, :all
        params.require(:subscription_plan).permit!
      end
    end

    #------------------------------------------------------------------------------
    def subscription_params
      params.require(:subscription).permit! if can? :manage_subscriptions, :all
    end

    #------------------------------------------------------------------------------
    def make_payment_billing_address_params
      if can? :manage_subscriptions, :all
        params[:make_payment].require(:billing_address).permit!
      end
    end
  end
end
