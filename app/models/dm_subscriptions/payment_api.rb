# frozen_string_literal: true

# Abstract out access to ActiveMerchant and various processor specific
# helper functions.
#------------------------------------------------------------------------------
class DmSubscriptions::PaymentApi
  #------------------------------------------------------------------------------
  def self.api
    case Account.current.preferred_subscription_processor
    when 'stripe'
      DmSubscriptions::PaymentApiStripe.new
    when 'paymill'
      DmSubscriptions::PaymentApiPaymill.new
    else
      DmSubscriptions::PaymentApiBogus.new
    end
  end

  #------------------------------------------------------------------------------
  def gateway
    ActiveMerchant::Billing::Base.gateway(processor).new(login: '', password: '')
  end

  #------------------------------------------------------------------------------
  def processor
    ''
  end

  #------------------------------------------------------------------------------
  def paymill?
    false
  end

  #------------------------------------------------------------------------------
  def stripe?
    false
  end

  # indicates if this gateway uses a javascript bridge
  #------------------------------------------------------------------------------
  def javascript_bridge?
    false
  end
end
