# frozen_string_literal: true

class SubscriptionPayment < ApplicationRecord
  monetize        :amount_cents, with_model_currency: :amount_currency

  belongs_to      :subscription
  belongs_to      :subscriber, polymorphic: true
  belongs_to      :affiliate, class_name: 'SubscriptionAffiliate', foreign_key: 'subscription_affiliate_id'

  before_create   :set_info_from_subscription
  before_create   :calculate_affiliate_amount
  after_create    :set_invoice_num
  after_create    :generate_invoice
  after_create    :send_receipt

  default_scope   { where(account_id: Account.current.id) }

  #------------------------------------------------------------------------------
  def self.stats
    {
      last_month: Money.new(where(created_at: (1.month.ago.beginning_of_month..1.month.ago.end_of_month)).calculate(:sum, :amount_cents), Account.current.preferred_default_currency),
      this_month: Money.new(where(created_at: (Time.now.beginning_of_month..Time.now.end_of_month)).calculate(:sum, :amount_cents), Account.current.preferred_default_currency),
      last_30: Money.new(where(created_at: (1.month.ago..Time.now)).calculate(:sum, :amount_cents), Account.current.preferred_default_currency)
    }
  end

  protected

  # Generate a sequential invoice number for this invoice and account
  #------------------------------------------------------------------------------
  def set_invoice_num
    Account.transaction do
      #--- lock should be the first thing done
      account = Account.where(id: account_id).lock(true).first
      update_attribute(:invoice_num, account.next_invoice_num)
      Account.increment_counter(:next_invoice_num, account.id)
    end
  end

  #------------------------------------------------------------------------------
  def generate_invoice
    filename = DmSubscriptions::InvoiceGeneratorService.new(self).save_to_pdf
    update_attribute(:invoice_filename, filename)
  rescue Exception => e
    #--- make sure a problem generating invoice doesn't keep payment from being recorded
    Rails.logger.error("\nProblem creating payment invoice: \n#{e.message}\n\t#{e.backtrace.join("\n\t")}\n")
  end

  #------------------------------------------------------------------------------
  def set_info_from_subscription
    self.subscriber = subscription.subscriber
    self.affiliate = subscription.affiliate
  end

  #------------------------------------------------------------------------------
  def calculate_affiliate_amount
    return unless affiliate

    self.affiliate_amount = amount * affiliate.rate
  end

  #------------------------------------------------------------------------------
  def send_receipt
    return unless amount.positive?

    if setup?
      SubscriptionNotifier.setup_receipt(self).deliver_now
    elsif misc?
      SubscriptionNotifier.misc_receipt(self).deliver_now
    else
      SubscriptionNotifier.charge_receipt(self).deliver_now
    end
    true
  end
end
