# frozen_string_literal: true

class SubscriptionAddress
  include ActiveMerchant::Validateable

  attr_accessor :address1, :address2, :city, :state, :zip, :country, :first_name, :last_name, :phone

  #------------------------------------------------------------------------------
  def to_activemerchant
    %i[address1 address2 city state zip country first_name last_name phone].each_with_object({}) do |field, h|
      h[field] = send(field)
    end
  end

  #------------------------------------------------------------------------------
  def validate
    %i[address1 city state zip first_name last_name].each do |field|
      errors.add field, 'cannot be blank' if send(field).blank?
    end
  end
end
