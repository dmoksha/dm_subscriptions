$ ->
  if $('meta[name="stripe-key"]').length
    window.subscription_manager = new window.StripeSubscriptionManager

  if $('meta[name="paymill-key"]').length
    window.subscription_manager = new window.PaymillSubscriptionManager