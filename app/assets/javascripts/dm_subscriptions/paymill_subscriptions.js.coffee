window.PaymillSubscriptionManager = class PaymillSubscriptionManager
  constructor: ->
    @setupForm()

  setupForm: ->
    I18n.locale = $('#credit-card-fields').data('locale')
    @initPayframe()

    # Form submit is disabled by default to prevent submission without javascript
    $('#subscription_billing input[type=submit]').attr('disabled', false)

    $('#subscription_billing').submit =>
      $('#subscription_billing input[type=submit]').attr('disabled', true)
      @processCardFrame()
      false

  initPayframe: ->
    options = { lang: I18n.locale }
    paymill.embedFrame('credit-card-fields', options, @initPayframeCallback.bind(this))

  initPayframeCallback: (error) ->
    if error
      console.log(error.apierror, error.message)
    else
      # nothing

  processCardFrame: ->
    card =
      amount_int:       $('#card_amount_int').val()
      currency:         $('#currency').val()
      email:            $('#email').val()

    paymill.createTokenViaFrame(card, @handlePaymillFrameResponse.bind(this))

  handlePaymillFrameResponse: (error, result) ->
    if error
      # Token could not be created, check error object for reason.
      @errorHandler(error)
    else
      $('#paymillToken').val(result.token)
      $('#card_number').val('')
      $('#card_code').val('')
      $('#credit_card_brand').val(result.brand)
      $('#credit_card_country').val(result.binCountry)
      $('#credit_card_ip').val(result.ip)
      $('#credit_card_ip_country').val(result.ipCountry)

      $('#subscription_billing')[0].submit()

  errorHandler: (error) ->
    text = "Unknown Error"
    if error.jserror
      text = I18n['subscriptions'][error.jserror]
    else if error.apierror
      text = error.message || I18n['subscriptions'][error.apierror]

    $('#form_error').text(text).show()
    $('input[type=submit]').attr('disabled', false)
