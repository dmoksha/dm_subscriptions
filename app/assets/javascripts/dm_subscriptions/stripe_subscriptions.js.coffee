window.StripeSubscriptionManager = class StripeSubscriptionManager
  constructor: ->
    Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'))
    @setupForm()

  setupForm: ->
    #--- Form submit is disabled by default to prevent submission without javascript
    $('#subscription_billing input[type=submit]').attr('disabled', false)

    $('#subscription_billing').submit =>
      $('#subscription_billing input[type=submit]').attr('disabled', true)
      if $('#card_number').length
        @processCard()
        false
      else
        true

  processCard: ->
    card =
      number:           $('#card_number').val()
      cvc:              $('#card_code').val()
      exp_month:        $('#card_month').val()
      exp_year:         $('#card_year').val()
      name:             $('#creditcard_first_name').val()+' '+$('#creditcard_last_name').val()
      address_line1:    $('#address_address1').val()
      address_line2:    $('#address_address2').val()
      address_state:    $('#address_state').val()
      address_zip:      $('#address_zip').val()
      address_country:  $('#address_country').val()

    Stripe.createToken(card, @handleStripeResponse)

  handleStripeResponse: (status, response) ->
    if status == 200
      $('#card_token').val(response.id)
      $('#card_number').val('')
      $('#card_code').val('')
      $('#subscription_billing')[0].submit()
    else
      # $('#form_error').text(I18n['subscriptions'][error.apierror]).show()
      $('#form_error').text(response.error.message).show()
      $('#subscription_billing input[type=submit]').attr('disabled', false)