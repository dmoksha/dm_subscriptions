# frozen_string_literal: true

class SubscriptionNotifier < DmCore::SiteMailer
  include ActionView::Helpers::NumberHelper
  helper DmCore::LiquidHelper
  helper DmCore::UrlHelper
  helper DmCore::AccountHelper
  helper DmUtilities::DateHelper

  #------------------------------------------------------------------------------
  def setup_environment(obj)
    if obj.is_a?(SubscriptionPayment)
      @subscription = obj.subscription
      @amount       = obj.amount
    elsif obj.is_a?(Subscription)
      @subscription = obj
    end
    @subscriber = @subscription.subscriber
    @account    = @subscription.account
    @bcc        = @account.preferred_subscription_bcc_email
    @billing_profile_url = main_app.edit_profile_billing_url(locale: I18n.locale, host: Account.current.url_host)
    headers = { 'Reply-To' => @account.preferred_smtp_from_email, 'Return-Path' => @account.preferred_smtp_from_email }
  end

  #------------------------------------------------------------------------------
  def trial_expiring(subscription)
    setup_environment(subscription)
    mail(to: @subscriber.email, subject: "Trial Expiring: Trial period expiring for #{@account.company_name}", bcc: @bcc, theme: @account.account_prefix) do |format|
      format.text { render 'dm_subscriptions/subscription_notifier/trial_expiring.text' }
      format.html { render 'dm_subscriptions/subscription_notifier/trial_expiring.html' }
    end
  end

  #------------------------------------------------------------------------------
  def charge_receipt(subscription_payment)
    setup_environment(subscription_payment)
    if subscription_payment.invoice_filename
      attachments[subscription_payment.invoice_filename] = File.read(File.join(DmSubscriptions::InvoiceGeneratorService.save_path, subscription_payment.invoice_filename))
    end
    @manual = true if subscription_payment.payment_processor == 'manual'
    mail(to: @subscriber.email, subject: "Invoice #{subscription_payment.invoice_num}: #{@account.company_name} subscription receipt", bcc: @bcc, theme: @account.account_prefix) do |format|
      format.text { render 'dm_subscriptions/subscription_notifier/charge_receipt.text' }
      format.html { render 'dm_subscriptions/subscription_notifier/charge_receipt.html' }
    end
  end

  #------------------------------------------------------------------------------
  def setup_receipt(subscription_payment)
    setup_environment(subscription_payment)
    mail(to: @subscriber.email, subject: "Invoice #{subscription_payment.invoice_num}: #{@account.company_name} invoice", bcc: @bcc, theme: @account.account_prefix) do |format|
      format.text { render 'dm_subscriptions/subscription_notifier/setup_receipt.text' }
      format.html { render 'dm_subscriptions/subscription_notifier/setup_receipt.html' }
    end
  end

  #------------------------------------------------------------------------------
  def misc_receipt(subscription_payment)
    setup_environment(subscription_payment)
    mail(to: @subscriber.email, subject: "Invoice #{subscription_payment.invoice_num}: #{@account.company_name} invoice", bcc: @bcc, theme: @account.account_prefix) do |format|
      format.text { render 'dm_subscriptions/subscription_notifier/misc_receipt.text' }
      format.html { render 'dm_subscriptions/subscription_notifier/misc_receipt.html' }
    end
  end

  #------------------------------------------------------------------------------
  def charge_failure(subscription)
    setup_environment(subscription)
    mail(to: @subscriber.email, subject: "Renewal Failed: #{@account.company_name} subscription renewal failed",
         bcc: @bcc, theme: @account.account_prefix) do |format|
      format.text { render 'dm_subscriptions/subscription_notifier/charge_failure.text' }
      format.html { render 'dm_subscriptions/subscription_notifier/charge_failure.html' }
    end
  end

  #------------------------------------------------------------------------------
  def plan_changed(subscription)
    setup_environment(subscription)
    mail(to: @subscriber.email, subject: "Plan Changed: Your #{@account.company_name} plan has been changed", bcc: @bcc, theme: @account.account_prefix) do |format|
      format.text { render 'dm_subscriptions/subscription_notifier/plan_changed.text' }
      format.html { render 'dm_subscriptions/subscription_notifier/plan_changed.html' }
    end
  end
end
