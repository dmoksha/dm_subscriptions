# frozen_string_literal: true

module DmSubscriptions
  class AdminMenuInject
    #------------------------------------------------------------------------------
    def self.menu_items(user)
      menu = []
      if user.can?(:manage_subscriptions, :all)
        menu << { text: 'Subscriptions', icon_class: :subscriptions, link: DmSubscriptions::Engine.routes.url_helpers.admin_subscription_plans_path(locale: I18n.locale) }
      end
      menu
    end
  end
end
