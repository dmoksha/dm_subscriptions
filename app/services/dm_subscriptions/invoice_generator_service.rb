# frozen_string_literal: true

module DmSubscriptions
  class InvoiceGeneratorService
    #------------------------------------------------------------------------------
    def initialize(subscription_payment)
      @subscription_payment = subscription_payment
    end

    # Generate a PDF invoice and store in the theme's directory.
    # returns the filename to store as a reference
    #------------------------------------------------------------------------------
    def save_to_pdf(filename = '')
      if filename.blank?
        filename = invoice_filename(@subscription_payment.created_at, @subscription_payment.invoice_num)
      end
      path      = File.join(InvoiceGeneratorService.save_path, filename)
      html      = generate_html(@subscription_payment)
      pdf       = WickedPdf.new.pdf_from_string(html)

      File.open(path, 'wb') do |file|
        file << pdf
      end

      filename
    end

    # filename like "2014-02-07_1034_mydomain.com.pdf"
    #------------------------------------------------------------------------------
    def invoice_filename(datetime, invoice_num)
      "#{datetime.to_date.to_formatted_s(:db)}_#{invoice_num}_#{Account.current.domain}.pdf"
    end

    # Render the html invoice template
    #------------------------------------------------------------------------------
    def generate_html(_subscription_payment)
      starting_2018 = Time.now.to_date > Date.new(2017, 12, 31)
      details       = {}

      details[:subscription_payment] = @subscription_payment
      details[:invoice_num]          = @subscription_payment.invoice_num
      details[:invoice_date]         = @subscription_payment.created_at
      details[:total]                = @subscription_payment.amount
      details[:tax_rate]             = starting_2018 ? 0.0 : 19.0
      details[:tax_amount]           = starting_2018 ? 0.0 : details[:total] - (details[:total] / (1 + details[:tax_rate] / 100)).round(2)
      details[:item_amount]          = details[:total] - details[:tax_amount]
      details[:customer_account]     = @subscription_payment.subscription.subscriber.email
      details[:next_renewal_at]      = @subscription_payment.subscription.next_renewal_at
      details[:is_first_payment]     = (@subscription_payment.subscription.subscription_payments.count == 1)
      details[:plan_fee]             = @subscription_payment.subscription.amount
      details[:plan_name]            = @subscription_payment.subscription.subscription_plan.name

      if @subscription_payment.subscription.billing_address.present?
        details[:address]            = @subscription_payment.subscription.billing_address.to_html(@subscription_payment.subscription.name_on_card)
      else
        details[:address]            = nil
      end
      details[:description]          = @subscription_payment.description
      details[:renewal_period]       = @subscription_payment.subscription.renewal_period

      DmSubscriptions::ApplicationController.render(template: 'dm_subscriptions/subscription_notifier/invoice.html',
                                                    layout: false, locals: { details: details })
    end

    # where to store the generated pdfs
    #------------------------------------------------------------------------------
    def self.save_path
      File.join(Rails.root, 'protected_assets', 'invoices', Account.current.account_prefix)
    end
  end
end
