# frozen_string_literal: true

# This is for configuring the Paymill Ruby gem https://github.com/dkd/paymill-ruby
# which is used for subscriptions.  It uses a class variable, Paymill.api_key,
# to hold the Paymill private_key.  Here we modify the class to use the value
# hanging off of the Account object, as long as the Paymill @@api_key is nil
#------------------------------------------------------------------------------
module Paymill
  def self.api_key
    @@api_key || Rails.application.secrets[Account.current.account_prefix]['paymill_private_key']
  end
end
