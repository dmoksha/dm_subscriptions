# frozen_string_literal: true

require 'spec_helper'
include ActionView::Helpers::NumberHelper

describe SubscriptionPlan do
  setup_account

  fixtures :subscription_plans

  before(:each) do
    @plan = create(:subscription_plan)
  end

  it 'should return a formatted name' do
    expect(@plan.to_s).to eq("#{@plan.name} - #{@plan.amount.format} / month")
  end

  it 'should return the name for URL params' do
    # @plan.to_param.should == @plan.name
    expect(@plan.slug).to eq('basic')
  end

  it 'should return a discounted amount with a discount' do
    @plan.discount = SubscriptionDiscount.new(valid_discount)
    expect(@plan.amount).to eq(@plan.price - 5.to_money('EUR'))
  end

  it 'should not return a discounted amount if the discount does not apply to the periodic charge' do
    @plan.discount = SubscriptionDiscount.new(valid_discount(apply_to_recurring: false))
    expect(@plan.amount).to eq(@plan.price)
  end

  it 'should return the full amount with a discount when suppressing the discount' do
    @plan.discount = SubscriptionDiscount.new(valid_discount)
    expect(@plan.amount(false)).to eq(@plan.price)
  end

  it 'should return the full amount without a discount' do
    expect(@plan.discount).to be_nil
    expect(@plan.amount).to eq(@plan.price)
  end

  it 'should return a discounted setup amount' do
    @plan.discount        = SubscriptionDiscount.new(valid_discount(apply_to_setup: true))
    @plan.setup_price     = 10
    @plan.price_currency  = 'EUR'
    expect(@plan.setup_amount).to eq(@plan.setup_price - 5.to_money('EUR'))
  end

  it 'should not return a discounted setup amount if the discount does not apply to the setup fee' do
    @plan.discount = SubscriptionDiscount.new(valid_discount(apply_to_setup: false))
    @plan.setup_price = 10
    expect(@plan.setup_amount).to eq(@plan.setup_price)
  end

  it 'should return the default trial period without a discount' do
    expect(@plan.trial_period).to eq(1)
  end

  it 'should return the default trial period with a discount with the default trial period extension' do
    @plan.discount = SubscriptionDiscount.new(valid_discount)
    expect(@plan.trial_period).to eq(1)
  end

  it 'should return a longer trial period with a discount with trial period extension' do
    @plan.discount = SubscriptionDiscount.new(valid_discount(trial_period_extension: 2))
    expect(@plan.trial_period).to eq(3)
  end
end
