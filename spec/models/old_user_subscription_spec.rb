# frozen_string_literal: true
# require 'spec_helper'
#
# describe DmSubscriptions do
#   before do
#     Account.current = FactoryBot.create(:paymill_account)
#     @sample_offer   = 'offer_e095735642a9ad4992ab'  # daily test offer, created in advance
#   end
#
#   describe DmSubscriptions::UserSubscription do
#
#     it '#save_with_payment (with Paymill)' do
#       @api            = DmSubscriptions::PaymentApiPaymill
#       card_token      = @api::Card.test_token
#       user            = create(:user)
#       user_subscription = user.build_current_subscription
#       user_subscription.save_with_payment(card_token: card_token, plan_id: @sample_offer)
#       expect(DmSubscriptions::UserSubscription.all.length).to eq 1
#     end
#
#     # it "is invalid without a subscription plan" do
#     #   # sub = DmSubscriptions::UserSubscription.new(last_4_digits: '1234')
#     #   # expect(sub).not_to be_valid
#     # end
#   end
# end
