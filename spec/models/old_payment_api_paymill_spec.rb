# frozen_string_literal: true
# require 'spec_helper'
#
# describe DmSubscriptions do
#   before do
#     Account.current = FactoryBot.create(:paymill_account)
#     @api            = DmSubscriptions::PaymentApiPaymill
#     @sample_offer   = 'offer_e095735642a9ad4992ab'  # daily test offer, created in advance
#   end
#
#   describe DmSubscriptions::PaymentApiPaymill do
#     it "has valid paymill api and keys" do
#       expect(Account.current.preferred_paymill_public_key).to_not eq nil
#       expect(Account.current.preferred_paymill_private_key).to_not eq nil
#       expect(@api).to eq DmSubscriptions::PaymentApiPaymill
#     end
#
#     it "creates a valid customer/client object" do
#       client_id = @api::Customer.create(email: 'joe@chickenshack.net', description: "Joe's Chicken Shack")
#       expect(client_id).to eq @api::Customer.find(client_id).id
#       Paymill::Client.delete client_id
#     end
#
#     it "has subscription plans" do
#       plans = @api.plans
#       expect(plans.detect {|x| x[:vendor_plan_id] == @sample_offer}).to be_truthy
#     end
#
#     it "has a test credit card token" do
#       card_token = @api::Card.test_token
#       expect(card_token[0,4]).to eq('tok_')
#     end
#
#     it "returns a subscription id, starting 'sub_' when successful" do
#       card_token      = @api::Card.test_token
#       client_id       = @api::Customer.create(email: 'joe@chickenshack.net')
#       subscription_id = @api::Subscription.create(plan: @sample_offer, customer_token: client_id, card_token: card_token)
#       expect(subscription_id[0,4]).to eq('sub_')
#
#       #--- cleanup
#       Paymill::Subscription.delete subscription_id
#       Paymill::Client.delete client_id
#     end
#   end
# end
