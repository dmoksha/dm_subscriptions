# frozen_string_literal: true

require 'spec_helper'
require 'stripe'
include ActiveMerchant::Billing

if ENV['REMOTE']
  describe DmSubscriptions do
    setup_account
    ActiveMerchant::Billing::Base.mode = :test unless Rails.env.production?

    #------------------------------------------------------------------------------
    describe DmSubscriptions::PaymentApiPaymill do
      before(:all) do
        Account.current = FactoryBot.create(:paymill_account)
        Rails.application.secrets[Account.current.account_prefix] =
          { 'paymill_public_key' => '1008743811841c3b5514a7fc95a98cbc',
            'paymill_private_key' => '9f685b857c1ac21826396d4c3156d704' }
        @api = DmSubscriptions::PaymentApiPaymill.new
        @gateway = DmSubscriptions::PaymentApi.api.gateway
      end

      it 'has valid paymill api and keys' do
        expect(@api.class).to eq DmSubscriptions::PaymentApiPaymill
        expect(@api.public_key).to_not eq nil
        expect(@api.api_key).to_not eq nil
      end

      it 'has a test credit card token' do
        token = @api.class::Card.test_token

        expect(token[0, 4]).to eq('tok_')
      end

      it 'stores a credit card and returns only a token object' do
        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4111111111111111', verification_value: '123', month: '12', year: '2018')
        response    = @gateway.store(credit_card)

        expect(response.success?).to eq(true)
        expect(response.authorization[0, 4]).to eq('tok_')
      end

      it 'creates a new customer record and adds the credit card' do
        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4111111111111111', verification_value: '123', month: '12', year: '2018')
        response    = @gateway.store(credit_card, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)
        expect(response.authorization).to match(/^client_/)
        expect(response.params['data']['email']).to eq('test@example.com')
        expect(response.params['data']['description']).to eq('New client')
      end

      it 'adds a new card to an existing customer' do
        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4111111111111111', verification_value: '123', month: '12', year: '2018')
        response    = @gateway.store(credit_card, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)

        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4111111111111111', verification_value: '456', month: '01', year: '2020')
        response    = @gateway.store(credit_card, customer: response.authorization)

        expect(response.success?).to eq(true)
        expect(response.authorization).to match(/^client_/)
      end

      it 'creates a new customer record and adds the card token' do
        token     = @api.class::Card.test_token
        response  = @gateway.store(token, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)
        expect(response.authorization).to match(/^client_/)
        expect(response.params['data']['email']).to eq('test@example.com')
        expect(response.params['data']['description']).to eq('New client')
      end

      it 'adds a new token to an existing customer' do
        token     = @api.class::Card.test_token
        response  = @gateway.store(token, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)

        token     = @api.class::Card.test_token
        response  = @gateway.store(token, customer: response.authorization)

        expect(response.success?).to eq(true)
        expect(response.authorization).to match(/^client_/)
      end

      it 'adds a new token to an existing customer using update()' do
        token     = @api.class::Card.test_token
        response  = @gateway.store(token, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)

        token     = @api.class::Card.test_token
        response  = @gateway.update(response.authorization, token)

        expect(response.success?).to eq(true)
        expect(response.authorization).to match(/^client_/)
      end

      it 'delete a client' do
        token     = @api.class::Card.test_token
        response  = @gateway.store(token, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)

        response = @gateway.unstore(response.authorization)

        expect(response.success?).to eq(true)
      end

      it 'purchases using a client id' do
        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4111111111111111', verification_value: '123', month: '12', year: '2018')
        response    = @gateway.store(credit_card, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)

        response = @gateway.purchase(100, response.authorization, currency: 'EUR')

        expect(response.success?).to eq(true)
        expect(response.authorization).to match(/^tran_/)
      end

      it 'purchases using a client id and uses latest credit card' do
        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4111111111111111', verification_value: '123', month: '12', year: '2018')
        response    = @gateway.store(credit_card, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)

        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4111111111111111', verification_value: '123', month: '12', year: '2022')
        response    = @gateway.update(response.authorization, credit_card)

        expect(response.success?).to eq(true)

        response = @gateway.purchase(100, response.authorization, currency: 'EUR')

        expect(response.success?).to eq(true)
        expect(response.authorization).to match(/^tran_/)
        expect(response.params['data']['payment']['expire_year']).to eq('2022')
      end

      it 'extracts card number and expiration' do
        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4111111111111111', verification_value: '123', month: '02', year: '2018')
        response    = @gateway.store(credit_card, email: 'test@example.com', description: 'New client')
        expect(response.success?).to eq(true)
        card = @api.extract_card(response)

        expect(card[:display_number]).to eq('XXXX-XXXX-XXXX-1111')
        expect(card[:expiration]).to eq('02-2018')
      end
    end

    #------------------------------------------------------------------------------
    describe DmSubscriptions::PaymentApiStripe do
      before(:all) do
        Account.current = FactoryBot.create(:stripe_account)
        Rails.application.secrets[Account.current.account_prefix] =
          { 'stripe_public_key' => 'pk_test_X3vPaOmJjGO2I1AxR9d4D05Q',
            'stripe_private_key' => 'sk_test_Pr5GS8S684YZV0PsnbAzgtFU' }
        @api = DmSubscriptions::PaymentApiStripe.new
        @gateway = DmSubscriptions::PaymentApi.api.gateway
      end

      it 'has valid stripe api and keys' do
        expect(@api.class).to eq DmSubscriptions::PaymentApiStripe
        expect(@api.public_key).to_not eq nil
        expect(@api.api_key).to_not eq nil
      end

      it 'has a test credit card token' do
        token = @api.class::Card.test_token

        expect(token[0, 4]).to eq('tok_')
      end

      it 'creates a new customer record and adds the card token' do
        token     = @api.class::Card.test_token
        customer  = @gateway.store(token)

        expect(customer.params['error']).to eq nil
        expect(customer.token[0, 4]).to eq('cus_')
      end

      it 'extracts card number and expiration' do
        credit_card = ActiveMerchant::Billing::CreditCard.new(number: '4242424242424242', verification_value: '123', month: '01', year: '2024')
        response    = @gateway.store(credit_card, email: 'test@example.com', description: 'New client')

        expect(response.success?).to eq(true)

        card = @api.extract_card(response)

        expect(card[:display_number]).to eq('XXXX-XXXX-XXXX-4242')
        expect(card[:expiration]).to eq('01-2024')
      end
    end
  end
else
  describe '=== Remote PaymentApi Tests' do
    it 'are only run if REMOTE=1 environment variable is set' do
    end
  end
end
