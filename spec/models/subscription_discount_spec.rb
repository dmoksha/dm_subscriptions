# frozen_string_literal: true

require 'spec_helper'

describe SubscriptionDiscount do
  setup_account

  before(:each) do
    @discount = SubscriptionDiscount.new(amount: 5, code: 'foo')
  end

  it 'should be 0 for amounts less than or equal to zero' do
    expect(@discount.calculate(0.to_money)).to eq(0.to_money)
    expect(@discount.calculate(-1.to_money)).to eq(0.to_money)
  end

  it 'should not be greater than the subtotal' do
    expect(@discount.calculate(4.to_money)).to eq(4.to_money)
  end

  it 'should not be greater than the amount' do
    expect(@discount.calculate(5.to_money)).to eq(5.to_money)
    expect(@discount.calculate(6.to_money)).to eq(5.to_money)
  end

  it 'should calculate based on percentage' do
    @discount = SubscriptionDiscount.new(amount: 0.1, percent: true)
    expect(@discount.calculate(78.99.to_money).to_f).to eq(7.9)
  end

  it 'should not be available if starting in the future' do
    expect(SubscriptionDiscount.new(start_on: 1.day.from_now.to_date)).not_to be_available
  end

  it 'should not be available if ended in the past' do
    expect(SubscriptionDiscount.new(end_on: 1.day.ago.utc.to_date)).not_to be_available
  end

  it 'should be available if started in the past' do
    expect(SubscriptionDiscount.new(start_on: 1.week.ago.to_date)).to be_available
  end

  it 'should be available if ending in the future' do
    expect(SubscriptionDiscount.new(end_on: 1.week.from_now.to_date)).to be_available
  end

  it 'should be 0 if not available' do
    @discount = SubscriptionDiscount.new(amount: 0.1, percent: true, end_on: 1.week.ago.to_date)
    expect(@discount.calculate(10.to_money)).to eq(0.to_money)
  end

  it 'should be greater than another discount if the amount is greater than the other one' do
    @lesser_discount = SubscriptionDiscount.new(amount: @discount.amount - 1.to_money)
    expect(@discount > @lesser_discount).to be_truthy
    expect(@discount < @lesser_discount).to be_falsey
  end

  it 'should not be greater than another discount if the amount is less than the other one' do
    @greater_discount = SubscriptionDiscount.new(amount: @discount.amount + 1.to_money)
    expect(@discount > @greater_discount).to be_falsey
    expect(@discount < @greater_discount).to be_truthy
  end

  it 'should be greater than another discount if other one is nil' do
    expect(@discount > nil).to be_truthy
    expect(@discount < nil).to be_falsey
  end

  it 'should raise an error when comparing percent vs. amount discounts' do
    @other_discount = SubscriptionDiscount.new(amount: 0.1, percent: true)
    expect { @discount > @other_discount }.to raise_error(SubscriptionDiscount::ComparableError)
  end
end
