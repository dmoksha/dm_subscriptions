# frozen_string_literal: true

FactoryBot.define do
  factory(:subscription_discount) do
    name            { '5 off' }
    code            { 'bar' }
    amount_cents    { 500 }
    amount_currency { 'EUR' }
    percent         { false }
  end
end
