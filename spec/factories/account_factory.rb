# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    # id              1
    domain                      { 'test.example.com' }
    account_prefix              { 'test' }
    preferred_default_currency  { 'EUR' }

    factory :paymill_account do
      after(:build) do |account|
        account.preferred_subscription_processor  = 'paymill'
      end
    end

    factory :stripe_account do
      after(:build) do |account|
        account.preferred_subscription_processor  = 'stripe'
      end
    end
  end
end
