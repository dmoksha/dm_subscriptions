# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :user do
    email           { Faker::Internet.email }
    password        { 'something' }
    confirmed_at    { Time.now }
    user_profile
  end
end
