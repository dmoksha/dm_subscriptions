# frozen_string_literal: true

require 'spec_helper'

describe DmSubscriptions::InvoiceGeneratorService, type: :service do
  setup_account

  it '#save_path' do
    expect(described_class.save_path).to eq File.join(Rails.root, 'protected_assets', 'invoices', Account.current.account_prefix)
  end

  it '#invoice_filename' do
    datetime = Time.new(2017, 12, 21)
    expect(described_class.new(build(:subscription_payment)).invoice_filename(datetime, 1234)).to eq "2017-12-21_1234_#{Account.current.domain}.pdf"
  end
end
