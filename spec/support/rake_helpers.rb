# frozen_string_literal: true

module RakeHelpers
  def run_rake_task(task_name, *args)
    Rake::Task[task_name].reenable
    Rake.application.invoke_task("#{task_name}[#{args.join(',')}]")
  end

  def silence_output
    allow(main_object).to receive(:puts)
    allow(main_object).to receive(:print)
  end

  def main_object
    @main_object ||= TOPLEVEL_BINDING.eval('self')
  end
end
