# frozen_string_literal: true

# By placing all of MokshaCms's shared dependencies in this file and then loading
# it for each component's Gemfile, we can be sure that we're only testing just
# the one component of MokshaCms.
source 'https://rubygems.org'

gem 'rails', '5.0.7.2'

gem 'aced_rails',             git: 'https://github.com/digitalmoksha/aced_rails.git'
gem 'dm_core',                git: 'https://gitlab.com/dmoksha/moksha_cms.git', branch: '5-0-stable'
gem 'dm_preferences',         '~> 1.5'
gem 'themes_for_rails',       git: 'https://github.com/digitalmoksha/themes_for_rails.git'

#--- personal development gems
# local_path = '~/dev/_gitlab/dmoksha'
# gem 'dm_core',                path: "#{local_path}/moksha_cms/core"

gemspec

gem 'coffee-rails', '~> 4.2'
gem 'sass-rails', '~> 5.0'
gem 'therubyracer', platforms: :ruby
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'pry-byebug'
  gem 'sqlite3', '~> 1.3.6' # until an ActiveRecrod dependecy is removed
  gem 'thin' # use the Thin webserver during development

  gem 'factory_bot_rails', '~> 4.8'
  gem 'mocha', '~> 1.2', require: false
  gem 'rspec-rails', '~> 3.5'

  gem 'rubocop', '~> 0.52'
  gem 'rubocop-rspec', '~> 1.22'
end

group :test do
  gem 'capybara', '~> 2.13'
  gem 'database_cleaner', '~> 1.5'
  gem 'faker', '~> 1.7'
  gem 'launchy', '~> 2.4'
  gem 'rails-controller-testing'
  gem 'rspec-formatter-webkit'
  gem 'selenium-webdriver', '~> 3.3'
  gem 'syntax'
  gem 'webmock', '~> 3.7'
end
