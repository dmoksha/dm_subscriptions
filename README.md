# dm_subscriptions

Customized Subscription / Membership engine for Telugu Basha.

At the moment, this code is targeted to the PayMill gateway.  Hopefully, recurring billing will be added to ActiveMerchant for PayMill, and this code can be made more generic.
