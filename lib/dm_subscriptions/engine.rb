# frozen_string_literal: true

# require 'paymill'
# require 'stripe'
require 'active_merchant'
require 'dm_subscriptions/extensions/active_merchant'

module DmSubscriptions
  class Engine < ::Rails::Engine
    isolate_namespace DmSubscriptions

    initializer 'dm_subscriptions.helper' do |_app|
      ActionView::Base.include SubscriptionDiscountHelper
    end

    config.to_prepare do
      #--- require any concersn that need to be injected
      require_dependency 'dm_subscriptions/model_decorators'
      require_dependency 'dm_subscriptions/concerns/acts_as_subscription'
      require_dependency 'dm_subscriptions/concerns/user'
      require_dependency 'dm_subscriptions/concerns/application_controller'
      #
      # Dir.glob(Rails.root + "app/controllers/dm_subscriptions/**/concerns/*.rb").each do |c|
      #   require_dependency(c)
      # end
      # Dir.glob(Rails.root + "app/models/dm_subscriptions/**/concerns/*.rb").each do |c|
      #   require_dependency(c)
      # end
    end

    config.generators do |g|
      g.test_framework      :rspec, fixture: false
      g.fixture_replacement :factory_bot, dir: 'spec/factories'
      g.assets false
      g.helper false
    end
  end
end
