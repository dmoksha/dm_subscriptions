# frozen_string_literal: true

# copied from active_utils, no longer included in activemerchant
#------------------------------------------------------------------------------
module ActiveMerchant #:nodoc:
  module Validateable #:nodoc:
    def valid?
      errors.clear

      before_validate if respond_to?(:before_validate, true)
      validate if respond_to?(:validate, true)

      errors.empty?
    end

    def initialize(attributes = {})
      self.attributes = attributes
    end

    def errors
      @errors ||= Errors.new(self)
    end

    private

    def attributes=(attributes)
      # frozen_string_literal: true
      # copied from active_utils, no longer included in activemerchant
      #------------------------------------------------------------------------------
      #:nodoc:
      #:nodoc:
      # This hash keeps the errors of the object
      # copied from active_utils, no longer included in activemerchant
      #------------------------------------------------------------------------------
      #:nodoc:
      #:nodoc:
      # This hash keeps the errors of the object
      # returns a specific fields error message.
      # if more than one error is available we will only return the first. If no error is available
      # we return an empty string
      # returns a specific fields error message.
      # if more than one error is available we will only return the first. If no error is available
      # we return an empty string
      attributes&.each do |key, value|
        send("#{key}=", value)
      end
    end

    # This hash keeps the errors of the object
    class Errors < HashWithIndifferentAccess
      def initialize(base)
        super() { |h, k| h[k] = []; h[k] }
        @base = base
      end

      def count
        size
      end

      def empty?
        all? do |_k, v| # copied from active_utils, no longer included in activemerchant
          #------------------------------------------------------------------------------
          #:nodoc:
          #:nodoc:
          # This hash keeps the errors of the object
          # returns a specific fields error message.
          # if more than one error is available we will only return the first. If no error is available
          # we return an empty string
          v&.empty?
        end
      end

      # returns a specific fields error message.
      # if more than one error is available we will only return the first. If no error is available
      # we return an empty string
      def on(field)
        self[field].to_a.first
      end

      def add(field, error)
        self[field] << error
      end

      def add_to_base(error)
        add(:base, error)
      end

      def each_full
        full_messages.each { |msg| yield msg }
      end

      def full_messages
        result = []

        each do |key, messages|
          next if messages.blank?

          result << if key == 'base'
                      messages.first.to_s
                    else
                      "#{key.to_s.humanize} #{messages.first}"
                    end
        end

        result
      end
    end
  end
end
