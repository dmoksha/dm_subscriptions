# frozen_string_literal: true

#
# Override the AM class to make the purchase method
# accept a creditcard *or* a customer token as the second
# parameter, like the other stored-value gateways.
#
module ActiveMerchant #:nodoc:
  module Billing #:nodoc:
    class PaymillGateway < Gateway
      # OVERRIDE: original takes only a credit card object and creats a token
      # create either a credit card token, or a client/payment
      # use client/payments if description, email, or customer is specified.
      #------------------------------------------------------------------------------
      def store(creditcard, options = {})
        options[:money] ||= Money.new(0)
        if options[:customer] || options[:description] || options[:email] || creditcard.is_a?(String)
          store_with_customer(creditcard, options)
        else
          # original behaviour, create a normal token, no client/payment record
          save_card(creditcard, options)
        end
      end

      # make a purchase with either a credit card token, payment token, or client token
      #------------------------------------------------------------------------------
      def purchase(money, payment_method, options = {})
        # get the client record to pull it's stored payment objects
        if payment_method.to_s =~ /^client_/
          response = commit(:get, "clients/#{payment_method}", nil)
          if response.success?
            # if there are multiple payments, use the most recent one
            payments = response.params['data']['payment']
            payment  = payments.max { |x, y| x['created_at'] <=> y['created_at'] }
            payment_method = payment['id']
          end
        end

        action_with_token(:purchase, money, payment_method, options)
      end

      #------------------------------------------------------------------------------
      def update(customer_id, creditcard, options = {})
        options = options.merge(customer: customer_id)
        store(creditcard, options)
      end

      #------------------------------------------------------------------------------
      def unstore(customer_id, payment_id = nil, _options = {})
        if payment_id.nil?
          commit(:delete, "clients/#{customer_id}", nil)
        else
          commit(:delete, "payments/#{payment_id}", nil)
        end
      end

      private

      # create/update a client with new card details.  Returns client object
      #------------------------------------------------------------------------------
      def store_with_customer(creditcard, options)
        # create token if necessary
        response = create_token(creditcard, options)
        return unless response.success?

        token     = response.authorization
        post      = {}
        client_object = nil
        MultiResponse.run do |r|
          if options[:customer]
            # get existing client so that it can be returned
            r.process { client_object = commit(:get, "clients/#{options[:customer]}", nil) }
          else
            # create a new client
            post[:description]  = options[:description] if options[:description]
            post[:email]        = options[:email] if options[:email]
            r.process { client_object = commit(:post, 'clients', post) }
          end
          # create new payment object attached to client
          r.process { commit(:post, 'payments', token: token, client: r.authorization) }

          # retreive client again, so that it returns client record with new payment information, like Stripe api
          r.process { commit(:get, "clients/#{client_object.authorization}", nil) }
        end
      end

      # Create a token if a credit card is passed in.  Otherwise assume a valid token
      # is already there.  Return it in a Resonse object for consistentcy
      #------------------------------------------------------------------------------
      def create_token(creditcard, options = {})
        case creditcard
        when String
          Response.new(true, nil, {}, authorization: creditcard)
        else
          save_card(creditcard, options)
        end
      end

      # OVERRIDE:  original method was not taking into account that some api calls
      # do not return a response code.  Added check
      #------------------------------------------------------------------------------
      def response_from(raw_response)
        parsed = JSON.parse(raw_response)
        options = {
          authorization: authorization_from(parsed),
          test: (parsed['mode'] == 'test')
        }

        succeeded = (parsed['data'] == []) || (parsed['data']['response_code'].to_i == 20_000) || parsed['data']['response_code'].nil?
        Response.new(succeeded, response_message(parsed), parsed, options)
      end

      # OVERRIDE: orginal method did not remove nil from the array, resulting in a
      # semicolon after the id when there is no preauthorization.  Added '.compact'
      #------------------------------------------------------------------------------
      def authorization_from(parsed_response)
        parsed_data = parsed_response['data']
        return '' unless parsed_data.is_a?(Hash)

        [
          parsed_data['id'],
          parsed_data['preauthorization'].try(:[], 'id')
        ].compact.join(';')
      end

      # OVERRIDE: orginal method only set the token parameter, instead of checking
      # if the token is a payment.
      #------------------------------------------------------------------------------
      def purchase_with_token(money, card_token, options)
        post = {}

        add_amount(post, money, options)
        token_or_payment_param(post, card_token)
        post[:description]  = options[:order_id]
        post[:source]       = 'active_merchant'
        post[:client]       = options[:customer]
        commit(:post, 'transactions', post)
      end

      # OVERRIDE: orginal method only set the token parameter, instead of checking
      # if the token is a payment.
      #------------------------------------------------------------------------------
      def authorize_with_token(money, card_token, options)
        post = {}

        add_amount(post, money, options)
        token_or_payment_param(post, card_token)
        post[:description] = options[:order_id]
        post[:source]      = 'active_merchant'
        commit(:post, 'preauthorizations', post)
      end

      # api's that accept payment tokens must be specified in it's own parameter
      #------------------------------------------------------------------------------
      def token_or_payment_param(post, token)
        token =~ /^pay_/ ? (post[:payment] = token) : (post[:token] = token)
      end
    end
  end
end
