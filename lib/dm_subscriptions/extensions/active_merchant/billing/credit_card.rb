# frozen_string_literal: true

module ActiveMerchant #:nodoc:
  module Billing #:nodoc:
    class CreditCard
      # Returns or sets the zip code.
      #
      # @return [String]
      attr_accessor :zip

      def name=(full_name)
        self.first_name, self.last_name = full_name.strip.split(' ', 2)
      end

      def validate_essential_attributes #:nodoc:
        errors.add :first_name, 'cannot be empty'      if @first_name.blank?
        errors.add :name,       'cannot be empty'      if @first_name.blank?

        if @month.to_i.zero? || @year.to_i.zero?
          errors.add :month, 'is required'  if @month.to_i.zero?
          errors.add :year,  'is required'  if @year.to_i.zero?
        else
          unless valid_month?(@month)
            errors.add :month,      'is not a valid month'
          end
          errors.add :year, 'expired' if expired?
          unless expired? || valid_expiry_year?(@year)
            errors.add :year,       'is not a valid year'
          end
        end
      end
    end
  end
end
