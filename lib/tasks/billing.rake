# frozen_string_literal: true

namespace :scheduled do
  #------------------------------------------------------------------------------
  desc 'Daily task that will bill users and handle trial subscriptions'
  task run_billing: :environment do
    # Ensures that an exception doesn't kill the entire billing process
    def exception_catcher
      yield
    rescue Exception => e
      puts "\nException in Subscription billing: \n#{e.message}\n\t#{e.backtrace.join("\n\t")}\n"
      Rails.logger.error("\nException in Subscription billing: \n#{e.message}\n\t#{e.backtrace.join("\n\t")}\n")
    end

    def charge_subscription(sub, make_inactive = false)
      puts " --> #{sub.subscriber.email} : #{sub.payment_description}"
      unless sub.charge
        puts "!!!!! Failed: #{sub.errors.full_messages.first}"
        SubscriptionNotifier.charge_failure(sub).deliver
        sub.update_attribute(:state, 'inactive') if make_inactive
      end
    end

    puts "===== Running Billing for #{Time.now.to_date.to_formatted_s(:long)}"

    Subscription.unscoped.find_expiring_trials(5.days.from_now).each_with_index do |sub, index|
      puts('--- Send Expiring Trials Emails ---') if index == 0
      Account.current_by_prefix(sub.account.account_prefix)
      exception_catcher do
        puts " --> #{sub.subscriber.email} : Expires on #{sub.next_renewal_at.to_date.to_formatted_s(:long)}"
        SubscriptionNotifier.trial_expiring(sub).deliver
      end
    end

    # Trial subscriptions for which we have payment info.
    # This will always turn up empty unless we are collecting
    # payment info when creating an account.
    Subscription.unscoped.find_due_trials.each_with_index do |sub, index|
      puts('--- Due Trials ---') if index == 0
      Account.current_by_prefix(sub.account.account_prefix)
      exception_catcher { charge_subscription(sub) }
    end

    # Charge due subscriptions
    Subscription.unscoped.find_due.each_with_index do |sub, index|
      puts('--- Due today ---') if index == 0
      Account.current_by_prefix(sub.account.account_prefix)
      exception_catcher { charge_subscription(sub) }
    end

    # Subscriptions overdue for payment (2nd try)
    Subscription.unscoped.find_due(5.days.ago).each_with_index do |sub, index|
      puts('--- 2nd payment try ---') if index == 0
      Account.current_by_prefix(sub.account.account_prefix)
      exception_catcher { charge_subscription(sub, true) }
    end
  end
end
