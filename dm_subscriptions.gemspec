# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

#--- gem's version:
require 'dm_subscriptions/version'

#--- Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'dm_subscriptions'
  s.version     = DmSubscriptions::VERSION
  s.authors     = ['Brett Walker']
  s.email       = ['github@digitalmoksha.com']
  s.homepage    = ''
  s.summary     = 'Customized Subscription/Membership Engine'
  s.description = 'Customized Subscription/Membership Engine'

  s.files       = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'README.md']
  s.test_files  = Dir['spec/**/*']

  s.add_dependency 'rails', '> 4.2', '< 5.1'

  #--- dont' forget to add 'require' statement in engine.rb for any dependencies
  s.add_dependency 'activemerchant', '~> 1.79'
  s.add_dependency 'offsite_payments', '~> 2.2'
  s.add_dependency 'paymill', '~> 0.5.0'
  s.add_dependency 'stripe', '~> 1.34.0'

  #--- make sure the following gems are included in your app's Gemfile
  # gem 'dm_core', git: 'https://github.com/digitalmoksha/dm_core.git'
end
